<?php

error_reporting(E_ALL); //Enabling error reporting for all erros. Will make debugging easier.
ini_set('display_errors', 1);

header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Content-type: application/json');

require_once('queries.php'); //This is where your queries are

$data = isset( $_POST['json'] ) ? $_POST['json'] : 0; // Grab the json object that was POSTed

$fn = $data["fn"]; //Which function we calling?

switch($fn) { //Sort out which query to call

  case "helloWorld":
    $return = Query::helloWorld(); //Execute query
		echo json_encode($return); //Return results
		break;
  // Add your own queries below




  // -------------------------------------
}


?>
