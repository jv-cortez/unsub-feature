<?php

require_once('../../config/db_conn.php'); //Vince should have provide you with the config files for our database

class Query {

	static function helloWorld() {
		$return = array(
			"status" => 1, //Convention at Spacio is 1 = success, 0 = validation failure, anything else is an error of some sort
			"msg" => "Hello World!"
		);
		return $return;
	}
}

?>
