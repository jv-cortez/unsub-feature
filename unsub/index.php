<?php

	// error_reporting(E_ALL);
	// ini_set('display_errors', 1);

  header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");

  $foobar = $_GET["foobar"]; //An example of passing GET variables to the page

?>

<!DOCTYPE html>
<html lang="en" ng-app>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/img/favicon.png">

    <title>Spacio</title>

    <link href="/css/normalize.css" rel="stylesheet">
    <link href="/css/website.css" rel="stylesheet">
    <link href="/css/signup.css" rel="stylesheet">
    <link href="/fonts/fonts.css" rel="stylesheet">
  </head>

  <body>

    <style>
      @media (max-width: 800px) {
        .unsubscribe_form {
          width:90%;
        }
      }

      @media (min-width: 801px) {
        .unsubscribe_form {
          width:420px;
        }
      }
    </style>

    <div class="spacio-top-menu"><div class="login-overlay" style="display: none;">
    <div class="login-container">
      <div class="login-box">
          <div class="login-close-btn" onclick="hideLoginOptions()">CLOSE</div>
            <div class="large" style="margin-bottom:20px;text-align:center;">Login</div>
            <div class="normal" style="margin-bottom:20px;text-align:left;">Select the product you wish to login to:</div>
            <a href="https://spac.io/dashboard/"><div class="login-option">SPACIO FOR AGENTS</div></a>
            <a href="https://spac.io/teams/dashboard/"><div class="login-option">TEAM DASHBOARD</div></a>
            <a href="https://spac.io/brokerages/dashboard/"><div class="login-option">BROKERAGE DASHBOARD</div></a>
            <a href="/projects/dashboard/"><div class="login-option">SPACIO PROJECTS</div></a>
        </div>
    </div>
    </div><div class="web-menu">
      <div class="spacio-top-menu-button" id="top-btn-products" onclick="toggleProductOptions()">
        <div class="btn">PRODUCTS<div class="underline"></div></div>
      </div>
      <a href="/pricing/">
        <div class="spacio-top-menu-button" id="top-btn-pricing">
          <div class="btn">PRICING<div class="underline"></div></div>
        </div>
      </a>
      <a href="/">
        <div class="spacio-top-menu-logo">
          <img src="/img/favicon.png" height="40">
        </div>
      </a>
      <a href="/contact/">
        <div class="spacio-top-menu-button" id="top-btn-contact">
          <div class="btn">CONTACT<div class="underline"></div></div>
        </div>
      </a>
      <div class="spacio-top-menu-button" id="top-btn-request" onclick="showPopup('request')">
        <div class="btn">REQUEST DEMO<div class="underline"></div></div>
      </div>

      <div class="spacio-login-button-black" onclick="showLoginOptions()">
        <div class="btn">LOGIN<div class="underline"></div></div>
      </div>
    </div><div class="mobile-menu">
      <div class="spacio-mobile-menu-bar">
          <div class="spacio-top-menu-logo-mobile">
            <a href="/">
              <img src="/img/favicon.png" height="40">
            </a>
          </div>
          <div id="nav-icon" onclick="toggleMenu()" class="">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
          </div>
      </div>
      <div class="spacio-mobile-menu-content" style="display: none;">
          <div class="spacio-mobile-menu-button" onclick="toggleMobileProductOptions()">
            PRODUCTS
          </div>
          <div class="spacio-mobile-submenu products-submenu">
            <a href="/agents/">
              <div class="spacio-mobile-submenu-button">
                AGENTS
              </div>
            </a>
            <a href="/teams/">
              <div class="spacio-mobile-submenu-button">
                TEAMS
              </div>
            </a>
            <a href="/brokerages/">
                <div class="spacio-mobile-submenu-button">
              BROKERAGES
              </div>
              </a>
              <!-- <a href="/projects/">
                <div class="spacio-mobile-submenu-button">
              BUILDERS
              </div>
                </a> -->
          </div>

          <a href="/pricing/">
              <div class="spacio-mobile-menu-button">
                PRICING
              </div>
          </a>
          <a href="/contact/">
            <div class="spacio-mobile-menu-button">
              CONTACT
            </div>
          </a>

          <div style="margin-left:10px;margin-top:40px;width:calc(100% - 20px);" class="spacio-boxed-button-black" onclick="showLoginOptions('')">
            LOGIN
          </div>
          <div style="margin-left:10px;margin-top:20px;width:calc(100% - 20px);" class="spacio-boxed-button-black" onclick="showPopup('request')">
            REQUEST DEMO
          </div>
      </div>
    </div><div class="product-options small">
    <div class="product-options-bar">
      <a href="/agents/">
        <div class="product-option" style="margin-right:20px;">
          <div class="product-type-title">Agents</div>
          <div style="color:#999;">Automate Your Open<br>Houses</div>
        </div>
      </a>
      <a href="/teams/">
        <div class="product-option" style="margin-right:20px;">
          <div class="product-type-title">Teams</div>
          <div style="color:#999;">Consolidate Your Open<br>Houses</div>
        </div>
      </a>
      <a href="/brokerages/">
        <div class="product-option" style="margin-right:20px;">
          <div class="product-type-title">Brokerages</div>
          <div style="color:#999;">Paperless Open Houses for<br>All Your Agents</div>
        </div>
      </a>
      <!-- <a href="/projects/">
        <div class="product-option">
          <div class="product-type-title">Builders</div>
          <div style="color:#999;">Spacio Projects for Development Marketing</div>
        </div>
      </a> -->
    </div>
    <div class="product-options-overlay" onclick="toggleProductOptions()"></div>
    </div></div>
    <!-- TOP MENU END -->

    <div class="signup-scroll-one" style="background:#999;">
      <div class="overlay-flex" style="min-height:calc(100vh - 120px);">
        <!-- PUT YOUR HTML CODE HERE -->
        <form id="unsubscribe_Form" action="index.php" method="POST" style="height:auto;margin:0 auto;">
          <input type="hidden" name="fn" value="unsubscribeEmail">
          <input type="hidden" name="uid" value="">
          <div class="xlarge" style="text-align:center;margin-bottom:20px;">
              Unsubscribe
          </div>
          <div class="normal" style="text-align:left;margin-bottom:20px;line-height:1.4;">
            <div style="margin-bottom:15px;font-size:20px;">
            Please select the list you wish to unsubscribe <span style="color:#e77f24"></span> from:</div>
            <div style="margin-left:20px;">
              <input type="checkbox" value="true" id="unsub-onboard"> <span>Onboarding Emails</span><br>
              <input type="checkbox" value="true" id="unsub-campaign"> <span>Campaign Emails</span><br>
              <span class="unsub-claim" style="display: none;">
                <input type="checkbox" value="true" id="unsub-claim"> <span>Claim Emails</span>
              <br>
            </span></div>

          </div>

          <div style="width:100%;text-align:left">
            <input class="signup-submit-btn" type="submit" name="signup" value="UNSUBSCRIBE">
          </div>
        </form>
        <!-- PUT YOUR HTML CODE ABOVE -->
      </div>

    </div>
    <!-- SCROLL ONE END -->


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/js/jquery.js"></script>
    
    <script>
      var wsURL ="../php/webservice.php";

      $(document).ready(() => {
        email = email.replace(/\s+/g, '+').tolowerCase();
        insertAlertBox();

        if(type == "unclaimed") {
          $(".unsub-claim").show();
        } else {
          $(".unsub-claim").hide();
        }
        getUnsubObj();
      });

      getUnsubObj = () => { //This is an example of how you would make an AJAX call to the endpoint
      var _query = {};

      //Format form inputs into proper array
      _query["fn"] = "getUnsubObj";
      _query["email"] = email;
      _query["uid"] = uid;
      _query["type"] = type; //You can pass any number of index/value pair here

      loading(true, "Loading...");

      var json_data = _query;
          console.log("Outbound JSON Query: "+JSON.stringify(json_data));

      $.ajax({
              url: wsURL, //The path to the endpoint
              type:"POST",
              data: {json : json_data},
              success: function(data, textStatus, jqXHR){
                  console.log("Returned JSON Object: "+JSON.stringify(data)); //This will show you the payload that's returned
                  //Success! Do something with the data you get back from database

                  if(data.status == 1) {
              $("#unsub-onboard").prop('checked', data.unsubObj["onboard"]);
              $("#unsub-system").prop('checked', data.unsubObj["system"]);
              $("#unsub-claim").prop('checked', data.unsubObj["claim"]);
              $("#unsub-campaign").prop('checked', data.unsubObj["campaign"]);
                      //showAlert("info", "Unsubscribed", "You have unsubsribed to the selected mailing list.", "OK");
                    } else {
                      //showAlert("error", "Invalid Link", "Sorry, your unsubscribe link is invalid. Please contact support at support@spac.io");
                    }
                    loading(false);
                    //debugLog("Query Status: "+data.status);
              },
              error: function(jqXHR, textStatus, errorThrown) {
                  console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Unsubscribe Error: "+errorThrown);
                  alert("Didn't work!") //Error! Handle this gracefully
              },
              dataType: "json"
          });
      }

      //submit event handler for unsubscribbe form with ajax call
      $("form#unsubscribe_Form").submit((event) => {

        var _fields = $(this).serializeArray();
        var _query = {};

      _query["fn"] = "unsubscribeEmail2";
          _query["email"] = email;
      _query["uid"] = uid;
      _query["type"] = type;

      _query["system"] = "false";
      _query["onboard"] = "false";
      _query["campaign"] = "false";
      _query["claim"] = "false";

      if($("#unsub-onboard").is(":checked")) {
        _query["onboard"] = "true";
      }
      if($("#unsub-campaign").is(":checked")) {
        _query["campaign"] = "true";
      }
      if($("#unsub-claim").is(":checked")) {
        _query["claim"] = "true";
      }
          _query["email"] = _query["email"].tolowerCase();
          //Validate form inputs
          if (_query["email"] == "" || _query["email"] == null) {
            showAlert("error", "Invalid Email Format", "Please enter a valid email.");
          } else {

            //Clean up query object and end
            loading(true, "Loading...");
            //Salt + Hash password before sending to server

            var json_data = _query;
            console.log("Outbound JSON Query: " + JSON.stringify(json_data));
            $.ajax({
                url: wsURL_new, //The path to the endpoint
                type:"POST",
                data: {json : json_data},
                success: function(data, textStatus, jqXHR){
                    console.log("Returned JSON Object: "+JSON.stringify(data)); //This will show you the payload that's returned
                    //Success! Do something with the data you get back from database

                    if(data.status == 1) {
                        showAlert("info", "Unsubscribed", "You have unsubsribed to the selected mailing list.", "OK");
                      } else {
                        showAlert("error", "Invalid Link", "Sorry, your unsubscribe link is invalid. Please contact support at support@spac.io");
                      }
                      loading(false);
                      //debugLog("Query Status: "+data.status);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log("error:"+jqXHR.responseText+" test:"+textStatus+" error: Unsubscribe Error: "+errorThrown);
                    alert("Didn't work!") //Error! Handle this gracefully
                },
                dataType: "json"
            });
          }
          event.preventDefault();
        })
    </script>
  </body>
</html>
