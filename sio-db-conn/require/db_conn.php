<?php

// Load MongoDB library (https://github.com/mongodb/mongo-php-library) and connection details
require_once "vendor/autoload.php";
require_once 'db_config.php';

class Db_Conn {

    static protected $_instance;

    protected $db = null;

    final protected function __construct() {
  		$dbhost1 = DB_HOST_1;
  		$dbhost2 = DB_HOST_2;
  		$dbport = DB_PORT;
  		$dbname = DB_NAME;
  		$dbuser = DB_USER;
  		$dbpass = DB_PASS;

        $m = new MongoDB\Client("mongodb://$dbuser:$dbpass@$dbhost1:$dbport,$dbhost2:$dbport/$dbname");
        $this->db = $m->$dbname;
    }

    static public function getInstance() {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function getConnection() {
        return $this->db;
    }

    final protected function __clone() { }
}

?>
